import executeQuery from '../db/db.js'


//kaikkien kirjojen etsintä kutsu databaselle
const findAll = async () => {
    console.log('Requesting for all books...')
    const result = await executeQuery(
        'SELECT * FROM "books";'
    )
    console.log(`Found ${result.rows.length} books.`)
    return result
}


const findOne = async (id) => {
    console.log(`Requesting a book with id: ${id}...`)
    const result = await executeQuery(
        'SELECT * FROM "books" WHERE id = $1;', [id]
    )
    console.log(`Found ${result.rows.length} book(s).`)
    if (result.rowCount !== 1) {
        const err = new Error('Internal error')
        err.name = 'dbError'
        throw err
    }
    return result.rows[0]
}


//kirjan lisäyskutsu databaselle
const insertBook = async (book) => {
    const params = [book.name, book.author, book.read]
    console.log(`Inserting a new book ${params[0]}...`)
    const result = await executeQuery(
        'INSERT INTO "books" ("name", "author", "read") VALUES ($1, $2, $3);', 
        params
    )
    console.log(`New book ${params[0]} inserted successfully.`)
    return result
}


const updateBook = async (book) => {
    const params = [book.id, book.name, book.author, book.read]
    console.log(`Updating a book ${params[1]}...`)
    const result = await executeQuery(
        'UPDATE "books" SET name=$2, author=$3, read=$4 WHERE id=$1;', 
        params
    )
    console.log(`book ${params[1]} updated successfully.`)
    return result
}


const deleteBook = async (id) => {
    console.log(`Deleting a book ${id}...`)
    const result = await executeQuery(
        'DELETE FROM "books" WHERE id=$1;', [id]
    )
    console.log(`Book id:${id} deleted successfully.`)
    //console.log(result)
    return result
}


export default {
    findAll,
    findOne,
    insertBook,
    updateBook,
    deleteBook
}