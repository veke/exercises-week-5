import { Router } from 'express'
import booksService from '../services/booksService.js'

const router = Router()


router.get('/api/v1/books', async (_req, res) => {
  const books = await booksService.findAll()
  res.status(200).send(books)
})


router.get('/api/v1/books/:id', async (req, res) => {
  const book = await booksService.findOne(req.params.id)
  res.status(200).json(book)
})


router.post('/api/v1/books', async (req, res) => {
  const body = req.body
  const storedBook = await booksService.insertBook(body)
  //console.log(storedBook)
  if (storedBook.rowCount === 1) {
    res.status(201).send('Created')
  }
  else {
    throw new Error('Unexpected Error')
  }

})


router.put('/api/v1/books/:id', async (req, res) => {
  const book = { id: req.params.id, ...req.body }
  //console.log(req.body)
  const storedBook = await booksService.updateBook(book)
  if (storedBook.rowCount === 1) {
    res.status(200).send('Updated')
  }
  else {
    throw new Error('Unexpected Error')
  }
})


router.delete('/api/v1/books/:id', async (req, res) => {
  const book = await booksService.deleteBook(req.params.id)
  res.status(200).json('Deleted')
})


export default router