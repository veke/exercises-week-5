import express from 'express';
import dotenv from 'dotenv';
import 'express-async-errors';
import booksRouter from './api/booksRouter.js';
import {
  unknownEndpoint,
  errorHandler
} from './middlewares.js';
import { createTables } from './db/db.js';

process.env.NODE_ENV !== 'prod' && dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/', booksRouter);
app.use(unknownEndpoint);
app.use(errorHandler);

const APP_PORT = process.env.APP_PORT || 3000;

process.env.NODE_ENV !== 'test' && 
createTables() &&
app.listen(APP_PORT, () => {
  console.log(`Listening to ${APP_PORT}.`);
});

export default app