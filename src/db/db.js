import pg from 'pg'
import dotenv from 'dotenv'
import queries from './queries.js'

const isDev = process.env.NODE_ENV !== 'prod'
isDev && dotenv.config()

const { APP_PG_HOST, APP_PG_USER, APP_PG_DB, APP_PG_PASSWORD, APP_PG_PORT } = process.env

export const pool = new pg.Pool({
    host: APP_PG_HOST,
    user: APP_PG_USER,
    database: APP_PG_DB,
    password: APP_PG_PASSWORD,
    port: APP_PG_PORT,
    ssl: !isDev
})

// Parameters are expected to be passed as an array of primitives
export const executeQuery = async (query, parameters) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        //console.log(result)
        return result
    } catch (error) {
        console.error(error.stack)
        throw error
    } finally {
        client.release()
    }
}

export const createTables = async () => {
    await Promise.all([
        await executeQuery(queries.createBooksTable)
    ])
    console.log('Table initialized successfully.')
}


export default executeQuery