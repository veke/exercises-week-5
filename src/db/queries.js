const createBooksTable = `
    CREATE TABLE IF NOT EXISTS "books" (
	    "id" SERIAL PRIMARY KEY,
	    "name" VARCHAR(100) NOT NULL,
	    "author" VARCHAR(50) NOT NULL,
        "read" BOOLEAN
    );`

export default { 
    createBooksTable
}