import booksDao from '../dao/booksDao.js'


//kaikkien kirjojen etsintä kutsu daolle
const findAll = async () => {
    const books = await booksDao.findAll()
    //console.log(books.rows)
    return books.rows
}

const findOne = async (id) => {
    const book = await booksDao.findOne(id)
    return book
}

const validateBook = (book) => {
    return book['name'] && book['author']
}

//tämä toimii vain jos read on true???
const insertBook = async (book) => {
    //console.log(book)
    if (!validateBook(book)) {
        const err = new Error('Invalid book')
        err.name = 'UserError'
        throw err
    }
    else {
        return await booksDao.insertBook(book)
    }
}


const updateBook = async (book) => {
    if (!validateBook(book) || !book.id) {
        const err = new Error('Invalid book')
        err.name = 'UserError'
        throw err
    } else {
        return await booksDao.updateBook(book)
    }
}


const deleteBook = async (id) => {
    if(!id) {
        const err = new Error('Invalid book')
        err.name = 'UserError'
        throw err
    } else {
        return await booksDao.deleteBook(id)
    }
}


export default {findAll, findOne, insertBook, updateBook, deleteBook}