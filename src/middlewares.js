export const checkContentType = (req, res, next) => {
  if (req.method === 'POST' && req.headers['content-type'] !== 'application/json') {
      res.status(415).send('Unsupported Media Type')
  }
  next()
}

export const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'No one here' })
}

export const errorHandler = (error, _req, res, next) => {
    if (error.name === 'UserError') {
      res.status(400).send(error.message)
    }
    else if (error.name === 'dbError') {
      res.status(500).send({ error: 'Please try again' })
    }
    
    console.error(error)
    next(error)
}