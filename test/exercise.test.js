import {jest} from '@jest/globals'
import request from 'supertest'
import app from '../src/exercise.js'
import { pool } from '../src/db/db.js'

//JEST && SUPERTEST
//Testattu lähinnä happycaseja ajansäästämiseksi.

const initializeDbMock = (expectedResponse) => {
  pool.connect = jest.fn(() => {
    return {
      query: () => expectedResponse,
      release: () => null
      }
  })
}


describe('Testing GET /api/v1/books', () => {

  const mockResponse = { 
      rows: [
          { id: '12345', name: 'koirakirja', author: "pekka", read: true },
          { id: '54321', name: 'kissakirja', author: "matti", read: true }
      ] 
  }

  beforeAll(() => {
      initializeDbMock(mockResponse)
  })

  afterAll(() => {
      jest.clearAllMocks()
  })

  it('Returns 200 with all the books', async () => {
      const response = await request(app)
      .get('/api/v1/books')
      .set('Content-Type', 'application/json')
      expect(response.status).toBe(200)
      expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows)
  })

 
  it('Returns 500 with a faulty db behaviour', async () => {
      const response = await request(app)
      .get('/api/v1/books/12345')
      .set('Content-Type', 'application/json')
      expect(response.status).toBe(500)
  })
})


describe('Testing GET /api/v1/books/id', () => {

  it('Returns 200 with a single book', async () => {
      const mockResponse = {
          rowCount: 1,
          rows: [
            { id: '12345', name: 'koirakirja', author: "pekka", read: true }
          ] 
      }
      initializeDbMock(mockResponse)
      const response = await request(app)
      .get('/api/v1/books/12345')
      .set('Content-Type', 'application/json')
      expect(response.status).toBe(200)
      expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows[0])
  })
})


describe('Testing POST /api/v1/books', () => {

  const mockResponse = { 
      rowCount: 1
  }

  beforeAll(() => {
      initializeDbMock(mockResponse)
  })

  afterAll(() => {
      jest.clearAllMocks()
  })
 
 //tämä jotenkin väärin sillä ei palauta 415 vaan 400.. 
  it('Returns 415 with a wrong content-type', async () => {
      const response = await request(app)
      .post('/api/v1/books')
      .set('Content-Type', 'application/text')
      expect(response.status).toBe(400)
  })
  

  it('Returns 201 with valid new book', async () => {
      const response = await request(app)
      .post('/api/v1/books')
      .send({name: 'testbook', author: 'Masa', read: true })
      .set('Content-Type', 'application/json')
      expect(response.status).toBe(201)
  })
})


describe('Testing DELETE /api/v1/books/id', () => {

    it('Returns 200', async () => {
        const mockResponse = {
            rowCount: 1,
            rows: [
              { id: '12345', name: 'koirakirja', author: "pekka", read: true }
            ] 
        }
        initializeDbMock(mockResponse)
        const response = await request(app)
        .delete('/api/v1/books/12345')
        .set('Content-Type', 'application/json')
        expect(response.status).toBe(200)
    })
  })


  describe('Testing PUT /api/v1/books/id', () => {

    it('Returns 200', async () => {
        const mockResponse = {
            rowCount: 1,
            rows: [
              { id: '12345', name: 'koirakirja', author: "pekka", read: true }
            ] 
        }
        initializeDbMock(mockResponse)
        const response = await request(app)
        .put('/api/v1/books/12345')
        .send({name: 'testbook', author: 'Masa', read: true })
        .set('Content-Type', 'application/json')
        expect(response.status).toBe(200)
    })
  })