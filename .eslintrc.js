module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "commonjs": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2015
    },
    "rules": {
        "indent": ["error",2],
        "linebreak-style": ["error","windows"],
        "quotes": ["error","double"],
        "semi": ["error","always"],
        "no-var": ["error"],
        "no-console": 0
    }
};